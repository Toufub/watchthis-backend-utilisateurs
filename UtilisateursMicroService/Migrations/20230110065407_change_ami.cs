﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UtilisateursMicroService.Migrations
{
    /// <inheritdoc />
    public partial class changeami : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AmiId",
                schema: "public",
                table: "t_a_ami_ami",
                newName: "ami_id");

            migrationBuilder.RenameColumn(
                name: "UtilisateurId",
                schema: "public",
                table: "t_a_ami_ami",
                newName: "utl_id");

            migrationBuilder.RenameIndex(
                name: "IX_t_a_ami_ami_AmiId",
                schema: "public",
                table: "t_a_ami_ami",
                newName: "IX_t_a_ami_ami_ami_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ami_id",
                schema: "public",
                table: "t_a_ami_ami",
                newName: "AmiId");

            migrationBuilder.RenameColumn(
                name: "utl_id",
                schema: "public",
                table: "t_a_ami_ami",
                newName: "UtilisateurId");

            migrationBuilder.RenameIndex(
                name: "IX_t_a_ami_ami_ami_id",
                schema: "public",
                table: "t_a_ami_ami",
                newName: "IX_t_a_ami_ami_AmiId");
        }
    }
}
