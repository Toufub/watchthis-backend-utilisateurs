﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace UtilisateursMicroService.Migrations
{
    /// <inheritdoc />
    public partial class tentativeinversepropertyamis : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_a_amisde_ams",
                schema: "public");

            migrationBuilder.CreateTable(
                name: "t_a_ami_ami",
                schema: "public",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    UtilisateurId = table.Column<int>(type: "integer", nullable: false),
                    AmiId = table.Column<int>(type: "integer", nullable: false),
                    UtilisateurId1 = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_a_ami_ami", x => x.Id);
                    table.ForeignKey(
                        name: "FK_t_a_ami_ami_t_e_utilisateur_utl_UtilisateurId",
                        column: x => x.UtilisateurId,
                        principalSchema: "public",
                        principalTable: "t_e_utilisateur_utl",
                        principalColumn: "utl_id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_t_a_ami_ami_t_e_utilisateur_utl_UtilisateurId1",
                        column: x => x.UtilisateurId1,
                        principalSchema: "public",
                        principalTable: "t_e_utilisateur_utl",
                        principalColumn: "utl_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_t_a_ami_ami_UtilisateurId",
                schema: "public",
                table: "t_a_ami_ami",
                column: "UtilisateurId");

            migrationBuilder.CreateIndex(
                name: "IX_t_a_ami_ami_UtilisateurId1",
                schema: "public",
                table: "t_a_ami_ami",
                column: "UtilisateurId1");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_a_ami_ami",
                schema: "public");

            migrationBuilder.CreateTable(
                name: "t_a_amisde_ams",
                schema: "public",
                columns: table => new
                {
                    EstAmisDe = table.Column<int>(type: "integer", nullable: false),
                    UtilisateurId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_a_amisde_ams", x => new { x.EstAmisDe, x.UtilisateurId });
                    table.ForeignKey(
                        name: "FK_t_a_amisde_ams_t_e_utilisateur_utl_EstAmisDe",
                        column: x => x.EstAmisDe,
                        principalSchema: "public",
                        principalTable: "t_e_utilisateur_utl",
                        principalColumn: "utl_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_t_a_amisde_ams_t_e_utilisateur_utl_UtilisateurId",
                        column: x => x.UtilisateurId,
                        principalSchema: "public",
                        principalTable: "t_e_utilisateur_utl",
                        principalColumn: "utl_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_t_a_amisde_ams_UtilisateurId",
                schema: "public",
                table: "t_a_amisde_ams",
                column: "UtilisateurId");
        }
    }
}
