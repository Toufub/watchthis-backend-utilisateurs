﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace UtilisateursMicroService.Migrations
{
    /// <inheritdoc />
    public partial class removeidami : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_t_a_ami_ami_t_e_utilisateur_utl_UtilisateurId",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.DropForeignKey(
                name: "FK_t_a_ami_ami_t_e_utilisateur_utl_UtilisateurId1",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_a_ami_ami",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.DropIndex(
                name: "IX_t_a_ami_ami_UtilisateurId",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.DropIndex(
                name: "IX_t_a_ami_ami_UtilisateurId1",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.DropColumn(
                name: "Id",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.DropColumn(
                name: "UtilisateurId1",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.AddPrimaryKey(
                name: "pk_ami",
                schema: "public",
                table: "t_a_ami_ami",
                columns: new[] { "UtilisateurId", "AmiId" });

            migrationBuilder.CreateIndex(
                name: "IX_t_a_ami_ami_AmiId",
                schema: "public",
                table: "t_a_ami_ami",
                column: "AmiId");

            migrationBuilder.AddForeignKey(
                name: "fk_ami_utl",
                schema: "public",
                table: "t_a_ami_ami",
                column: "UtilisateurId",
                principalSchema: "public",
                principalTable: "t_e_utilisateur_utl",
                principalColumn: "utl_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_utl_ami",
                schema: "public",
                table: "t_a_ami_ami",
                column: "AmiId",
                principalSchema: "public",
                principalTable: "t_e_utilisateur_utl",
                principalColumn: "utl_id",
                onDelete: ReferentialAction.Restrict);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_ami_utl",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.DropForeignKey(
                name: "fk_utl_ami",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.DropPrimaryKey(
                name: "pk_ami",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.DropIndex(
                name: "IX_t_a_ami_ami_AmiId",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                schema: "public",
                table: "t_a_ami_ami",
                type: "integer",
                nullable: false,
                defaultValue: 0)
                .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn);

            migrationBuilder.AddColumn<int>(
                name: "UtilisateurId1",
                schema: "public",
                table: "t_a_ami_ami",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_a_ami_ami",
                schema: "public",
                table: "t_a_ami_ami",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_t_a_ami_ami_UtilisateurId",
                schema: "public",
                table: "t_a_ami_ami",
                column: "UtilisateurId");

            migrationBuilder.CreateIndex(
                name: "IX_t_a_ami_ami_UtilisateurId1",
                schema: "public",
                table: "t_a_ami_ami",
                column: "UtilisateurId1");

            migrationBuilder.AddForeignKey(
                name: "FK_t_a_ami_ami_t_e_utilisateur_utl_UtilisateurId",
                schema: "public",
                table: "t_a_ami_ami",
                column: "UtilisateurId",
                principalSchema: "public",
                principalTable: "t_e_utilisateur_utl",
                principalColumn: "utl_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_t_a_ami_ami_t_e_utilisateur_utl_UtilisateurId1",
                schema: "public",
                table: "t_a_ami_ami",
                column: "UtilisateurId1",
                principalSchema: "public",
                principalTable: "t_e_utilisateur_utl",
                principalColumn: "utl_id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
