﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UtilisateursMicroService.Migrations
{
    /// <inheritdoc />
    public partial class correctioninversepropertyamis : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_t_a_amisde_ams_t_e_utilisateur_utl_AmisDeId",
                schema: "public",
                table: "t_a_amisde_ams");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_a_amisde_ams",
                schema: "public",
                table: "t_a_amisde_ams");

            migrationBuilder.RenameColumn(
                name: "AmisDeId",
                schema: "public",
                table: "t_a_amisde_ams",
                newName: "EstAmisDe");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_a_amisde_ams",
                schema: "public",
                table: "t_a_amisde_ams",
                columns: new[] { "EstAmisDe", "UtilisateurId" });

            migrationBuilder.AddForeignKey(
                name: "FK_t_a_amisde_ams_t_e_utilisateur_utl_EstAmisDe",
                schema: "public",
                table: "t_a_amisde_ams",
                column: "EstAmisDe",
                principalSchema: "public",
                principalTable: "t_e_utilisateur_utl",
                principalColumn: "utl_id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_t_a_amisde_ams_t_e_utilisateur_utl_EstAmisDe",
                schema: "public",
                table: "t_a_amisde_ams");

            migrationBuilder.DropPrimaryKey(
                name: "PK_t_a_amisde_ams",
                schema: "public",
                table: "t_a_amisde_ams");

            migrationBuilder.RenameColumn(
                name: "EstAmisDe",
                schema: "public",
                table: "t_a_amisde_ams",
                newName: "AmisDeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_t_a_amisde_ams",
                schema: "public",
                table: "t_a_amisde_ams",
                column: "AmisDeId");

            migrationBuilder.AddForeignKey(
                name: "FK_t_a_amisde_ams_t_e_utilisateur_utl_AmisDeId",
                schema: "public",
                table: "t_a_amisde_ams",
                column: "AmisDeId",
                principalSchema: "public",
                principalTable: "t_e_utilisateur_utl",
                principalColumn: "utl_id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
