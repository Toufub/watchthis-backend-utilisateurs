﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UtilisateursMicroService.Migrations
{
    /// <inheritdoc />
    public partial class correctionamistable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_t_e_utilisateur_utl_t_e_utilisateur_utl_UtilisateurId1",
                schema: "public",
                table: "t_e_utilisateur_utl");

            migrationBuilder.DropIndex(
                name: "IX_t_e_utilisateur_utl_UtilisateurId1",
                schema: "public",
                table: "t_e_utilisateur_utl");

            migrationBuilder.DropColumn(
                name: "UtilisateurId1",
                schema: "public",
                table: "t_e_utilisateur_utl");

            migrationBuilder.CreateTable(
                name: "t_a_amisde_ams",
                schema: "public",
                columns: table => new
                {
                    AmisDeId = table.Column<int>(type: "integer", nullable: false),
                    UtilisateurId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_a_amisde_ams", x => x.AmisDeId);
                    table.ForeignKey(
                        name: "FK_t_a_amisde_ams_t_e_utilisateur_utl_AmisDeId",
                        column: x => x.AmisDeId,
                        principalSchema: "public",
                        principalTable: "t_e_utilisateur_utl",
                        principalColumn: "utl_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_t_a_amisde_ams_t_e_utilisateur_utl_UtilisateurId",
                        column: x => x.UtilisateurId,
                        principalSchema: "public",
                        principalTable: "t_e_utilisateur_utl",
                        principalColumn: "utl_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_t_a_amisde_ams_UtilisateurId",
                schema: "public",
                table: "t_a_amisde_ams",
                column: "UtilisateurId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_a_amisde_ams",
                schema: "public");

            migrationBuilder.AddColumn<int>(
                name: "UtilisateurId1",
                schema: "public",
                table: "t_e_utilisateur_utl",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_t_e_utilisateur_utl_UtilisateurId1",
                schema: "public",
                table: "t_e_utilisateur_utl",
                column: "UtilisateurId1");

            migrationBuilder.AddForeignKey(
                name: "FK_t_e_utilisateur_utl_t_e_utilisateur_utl_UtilisateurId1",
                schema: "public",
                table: "t_e_utilisateur_utl",
                column: "UtilisateurId1",
                principalSchema: "public",
                principalTable: "t_e_utilisateur_utl",
                principalColumn: "utl_id");
        }
    }
}
