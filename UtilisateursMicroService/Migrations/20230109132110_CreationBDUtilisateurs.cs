﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace UtilisateursMicroService.Migrations
{
    /// <inheritdoc />
    public partial class CreationBDUtilisateurs : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "public");

            migrationBuilder.CreateTable(
                name: "t_e_utilisateur_utl",
                schema: "public",
                columns: table => new
                {
                    utlid = table.Column<int>(name: "utl_id", type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    utlbetaseriesid = table.Column<string>(name: "utl_betaseriesid", type: "text", nullable: true),
                    utlnom = table.Column<string>(name: "utl_nom", type: "character varying(50)", maxLength: 50, nullable: false),
                    utlprenom = table.Column<string>(name: "utl_prenom", type: "character varying(50)", maxLength: 50, nullable: false),
                    utlmail = table.Column<string>(name: "utl_mail", type: "character varying(100)", maxLength: 100, nullable: false),
                    utlpwd = table.Column<string>(name: "utl_pwd", type: "character varying(64)", maxLength: 64, nullable: false),
                    utldatecreation = table.Column<DateTime>(name: "utl_datecreation", type: "Date", nullable: false, defaultValueSql: "now()"),
                    utlimageprofil = table.Column<string>(name: "utl_imageprofil", type: "character varying(100)", maxLength: 100, nullable: true),
                    utlnotificationactive = table.Column<bool>(name: "utl_notificationactive", type: "boolean", nullable: false),
                    UtilisateurId1 = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_t_e_utilisateur_utl", x => x.utlid);
                    table.ForeignKey(
                        name: "FK_t_e_utilisateur_utl_t_e_utilisateur_utl_UtilisateurId1",
                        column: x => x.UtilisateurId1,
                        principalSchema: "public",
                        principalTable: "t_e_utilisateur_utl",
                        principalColumn: "utl_id");
                });

            migrationBuilder.CreateTable(
                name: "t_j_categorisation_ctg",
                schema: "public",
                columns: table => new
                {
                    flmid = table.Column<int>(name: "flm_id", type: "integer", nullable: false),
                    utlid = table.Column<int>(name: "utl_id", type: "integer", nullable: false),
                    ctgcategorie = table.Column<string>(name: "ctg_categorie", type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("pk_categorisation", x => new { x.flmid, x.utlid });
                    table.ForeignKey(
                        name: "fk_ctg_utl",
                        column: x => x.utlid,
                        principalSchema: "public",
                        principalTable: "t_e_utilisateur_utl",
                        principalColumn: "utl_id");
                });

            migrationBuilder.CreateIndex(
                name: "IX_t_e_utilisateur_utl_UtilisateurId1",
                schema: "public",
                table: "t_e_utilisateur_utl",
                column: "UtilisateurId1");

            migrationBuilder.CreateIndex(
                name: "IX_t_e_utilisateur_utl_utl_mail",
                schema: "public",
                table: "t_e_utilisateur_utl",
                column: "utl_mail",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_t_j_categorisation_ctg_utl_id",
                schema: "public",
                table: "t_j_categorisation_ctg",
                column: "utl_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "t_j_categorisation_ctg",
                schema: "public");

            migrationBuilder.DropTable(
                name: "t_e_utilisateur_utl",
                schema: "public");
        }
    }
}
