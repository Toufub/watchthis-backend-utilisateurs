﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace UtilisateursMicroService.Migrations
{
    /// <inheritdoc />
    public partial class modifdeleteconstraint : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_ami_utl",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.DropForeignKey(
                name: "fk_utl_ami",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.AddForeignKey(
                name: "fk_ami_utl",
                schema: "public",
                table: "t_a_ami_ami",
                column: "utl_id",
                principalSchema: "public",
                principalTable: "t_e_utilisateur_utl",
                principalColumn: "utl_id");

            migrationBuilder.AddForeignKey(
                name: "fk_utl_ami",
                schema: "public",
                table: "t_a_ami_ami",
                column: "ami_id",
                principalSchema: "public",
                principalTable: "t_e_utilisateur_utl",
                principalColumn: "utl_id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "fk_ami_utl",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.DropForeignKey(
                name: "fk_utl_ami",
                schema: "public",
                table: "t_a_ami_ami");

            migrationBuilder.AddForeignKey(
                name: "fk_ami_utl",
                schema: "public",
                table: "t_a_ami_ami",
                column: "utl_id",
                principalSchema: "public",
                principalTable: "t_e_utilisateur_utl",
                principalColumn: "utl_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "fk_utl_ami",
                schema: "public",
                table: "t_a_ami_ami",
                column: "ami_id",
                principalSchema: "public",
                principalTable: "t_e_utilisateur_utl",
                principalColumn: "utl_id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
