﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text;
using UtilisateursMicroService.Models.EntityFramework;
using UtilisateursMicroService.Models.Repository;
using UtilisateursMicroService.Services;

namespace UtilisateursMicroService.Models.DataManager
{
    public class UtilisateurManager : IUtilisateurRepository<Utilisateur>
    {
        readonly UtilisateursContext? utilisateursContext;
        public UtilisateurManager() { }
        public UtilisateurManager(UtilisateursContext context)
        {
            utilisateursContext = context;
        }
        public async Task<ActionResult<IEnumerable<Utilisateur>>> GetAll()
        {
            return await utilisateursContext.Utilisateurs.ToListAsync();
        }

        public async Task<ActionResult<IEnumerable<Utilisateur>>> Search(string search)
        {
            return await utilisateursContext.Utilisateurs
                .Where(u => u.Mail.Contains(search) || u.Prenom.Contains(search) || u.Nom.Contains(search))
                .Distinct()
                .ToListAsync();
        }
        public async Task<ActionResult<Utilisateur>> GetById(int id)
        {
            return await utilisateursContext.Utilisateurs
                .Include(u => u.CategorisationsUtilisateur)
                .FirstOrDefaultAsync(u => u.UtilisateurId == id);
        }
        public async Task<ActionResult<Utilisateur>> GetByString(string mail)
        {
            return await utilisateursContext.Utilisateurs
                .Include(u => u.CategorisationsUtilisateur)
                .FirstOrDefaultAsync(u => u.Mail.ToUpper() == mail.ToUpper());
        }
        public async Task Add(Utilisateur entity)
        {
            entity.Pwd = Encoding.ASCII.GetString(PasswordManager.HashPassword(entity.Pwd));
            await utilisateursContext.Utilisateurs.AddAsync(entity);
            await utilisateursContext.SaveChangesAsync();
        }
        public async Task Update(Utilisateur utilisateur, Utilisateur entity)
        {
            utilisateursContext.Entry(utilisateur).State = EntityState.Modified;
            utilisateur.Nom = entity.Nom;
            utilisateur.Prenom = entity.Prenom;
            utilisateur.Mail = entity.Mail;
            utilisateur.BetaSeriesId = entity.BetaSeriesId;
            utilisateur.NotificationActive = entity.NotificationActive;
            utilisateur.ImageProfil = entity.ImageProfil;
            await utilisateursContext.SaveChangesAsync();
        }
        public async Task UpdatePwd(Utilisateur utilisateur, Utilisateur entity)
        {
            utilisateursContext.Entry(utilisateur).State = EntityState.Modified;
            utilisateur.Pwd = entity.Pwd;
            await utilisateursContext.SaveChangesAsync();
        }
        public async Task Delete(Utilisateur utilisateur)
        {
            utilisateursContext.Utilisateurs.Remove(utilisateur);
            await utilisateursContext.SaveChangesAsync();
        }
    }
}
