﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UtilisateursMicroService.Models.EntityFramework;
using UtilisateursMicroService.Models.Repository;

namespace UtilisateursMicroService.Models.DataManager
{
    public class AmiManager : IAmiRepository<Ami>
    {
        readonly UtilisateursContext? utilisateursContext;
        public AmiManager(UtilisateursContext context)
        {
            utilisateursContext = context;
        }

        public async Task Add(Ami entity)
        {
            await utilisateursContext.Amis.AddAsync(entity);
            await utilisateursContext.SaveChangesAsync();
        }

        public async Task Delete(Ami entity)
        {
            utilisateursContext.Amis.Remove(entity);
            await utilisateursContext.SaveChangesAsync();
        }

        public async Task<ActionResult<IEnumerable<Ami>>> GetAllAmis(int utilisateurId)
        {
            return await utilisateursContext.Amis
                .Include(a => a.SonAmi)
                .Where(a => a.UtilisateurId == utilisateurId)
                .ToListAsync();
        }

        public async Task<ActionResult<IEnumerable<Ami>>> GetAllAmisDe(int utilisateurId)
        {
            return await utilisateursContext.Amis
                .Include(a => a.UtilisateurAmi)
                .Where(a => a.AmiId == utilisateurId)
                .ToListAsync();
        }

        public async Task<ActionResult<Ami>> GetAmi(int utilisateurId, int amiId)
        {
            return await utilisateursContext.Amis
                            .Include(a => a.SonAmi)
                            .Include(a => a.UtilisateurAmi)
                            .Where(a => a.UtilisateurId == utilisateurId)
                            .Where(a => a.AmiId == amiId)
                            .FirstOrDefaultAsync();
        }
    }
}
