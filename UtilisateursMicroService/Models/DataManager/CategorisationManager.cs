﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UtilisateursMicroService.Models.EntityFramework;
using UtilisateursMicroService.Models.Repository;

namespace UtilisateursMicroService.Models.DataManager
{
    public class CategorisationManager : ICategorisationRepository<Categorisation>
    {
        readonly UtilisateursContext? utilisateursContext;
        public CategorisationManager(UtilisateursContext context)
        {
            utilisateursContext = context;
        }
        public async Task<ActionResult<IEnumerable<Categorisation>>> GetAll()
        {
            return await utilisateursContext.Categorisations.ToListAsync();
        }
        public async Task<ActionResult<IEnumerable<Categorisation>>> GetByUtilisateurId(int utilisateurId)
        {
            return await utilisateursContext.Categorisations
                .Where(c => c.UtilisateurId == utilisateurId)
                .ToListAsync();
        }
        public async Task<ActionResult<IEnumerable<Categorisation>>> GetByCategorieAndUtilisateurId(string categorie, int utilisateurId)
        {
            Categorie categorie_parse;
            Enum.TryParse<Categorie>(categorie, out categorie_parse);
            return await utilisateursContext.Categorisations
                            .Where(c => c.UtilisateurId == utilisateurId)
                            .Where(c => c.Categorie == categorie_parse)
                            .ToListAsync();
        }
        public async Task<ActionResult<Categorisation>> GetOneByFilmIdAndUtilisateurId(string filmId, int utilisateurId)
        {
            return await utilisateursContext.Categorisations
                            .Where(c => c.UtilisateurId == utilisateurId)
                            .Where(c => c.FilmId == filmId)
                            .FirstOrDefaultAsync();
        }

        public async Task Add(Categorisation entity)
        { 
            await utilisateursContext.Categorisations.AddAsync(entity);
            await utilisateursContext.SaveChangesAsync();
        }
        public async Task Update(Categorisation categorisation, Categorisation entity)
        {
            utilisateursContext.Entry(categorisation).State = EntityState.Modified;
            categorisation.Categorie = entity.Categorie;
            await utilisateursContext.SaveChangesAsync();
        }
        public async Task Delete(Categorisation categorisation)
        {
            utilisateursContext.Categorisations.Remove(categorisation);
            await utilisateursContext.SaveChangesAsync();
        }
    }
}
