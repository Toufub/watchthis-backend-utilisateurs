﻿namespace UtilisateursMicroService.Models
{
    public class LoginForm
    {
        public LoginForm(string mail, string pwd)
        {
            Mail = mail;
            Pwd = pwd;
        }
        public string Mail { get; set; }
        public string Pwd { get; set; }
    }
}
