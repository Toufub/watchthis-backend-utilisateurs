﻿namespace UtilisateursMicroService.Models
{
    public class ChangePassword
    {
        public string OldPwd { get; set; } = null!;
        public string NewPwd { get; set; } = null!;

    }
}
