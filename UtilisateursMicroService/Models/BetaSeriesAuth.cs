﻿namespace UtilisateursMicroService.Models
{
    public class BetaSeriesAuth
    {
        public BetaSeriesAuth(string login, string password)
        {
            this.client_id = "";
            this.login = login;
            this.password = password;
        }

        public string client_id { get; set; } = null!;
        public string login { get; set; } = null!;
        public string password { get; set; } = null!;
    }
}
