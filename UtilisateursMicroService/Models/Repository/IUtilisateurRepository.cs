﻿using Microsoft.AspNetCore.Mvc;
using UtilisateursMicroService.Models.EntityFramework;

namespace UtilisateursMicroService.Models.Repository
{
    public interface IUtilisateurRepository<TEntity>
    {
        Task<ActionResult<IEnumerable<TEntity>>> GetAll();
        Task<ActionResult<IEnumerable<TEntity>>> Search(string search);

        Task<ActionResult<TEntity>> GetById(int id);
        Task<ActionResult<Utilisateur>> GetByString(string mail);
        Task Add(TEntity entity);
        Task Update(TEntity entityToUpdate, TEntity entity);
        Task UpdatePwd(TEntity entityToUpdate, TEntity entity);
        Task Delete(TEntity entity);
    }
}
