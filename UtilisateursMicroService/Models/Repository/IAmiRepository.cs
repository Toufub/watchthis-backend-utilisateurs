﻿using Microsoft.AspNetCore.Mvc;
using UtilisateursMicroService.Models.EntityFramework;

namespace UtilisateursMicroService.Models.Repository
{
    public interface IAmiRepository<TEntity>
    {
        Task<ActionResult<IEnumerable<TEntity>>> GetAllAmis(int utilisateurId);
        Task<ActionResult<IEnumerable<TEntity>>> GetAllAmisDe(int utilisateurId);
        Task<ActionResult<TEntity>> GetAmi(int utilisateurId, int amiId);
        Task Add(TEntity entity);
        Task Delete(TEntity entity);
    }
}
