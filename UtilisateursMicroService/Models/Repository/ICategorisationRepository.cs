﻿using Microsoft.AspNetCore.Mvc;
using UtilisateursMicroService.Models.EntityFramework;

namespace UtilisateursMicroService.Models.Repository
{
    public interface ICategorisationRepository<TEntity>
    {
        Task<ActionResult<IEnumerable<TEntity>>> GetAll();
        Task<ActionResult<IEnumerable<Categorisation>>> GetByUtilisateurId(int utilisateurId);
        Task<ActionResult<IEnumerable<Categorisation>>> GetByCategorieAndUtilisateurId(String categorie, int utilisateurId);
        Task<ActionResult<Categorisation>> GetOneByFilmIdAndUtilisateurId(string filmId, int utilisateurId);
        Task Add(TEntity entity);
        Task Update(TEntity entityToUpdate, TEntity entity);
        Task Delete(TEntity entity);
    }
}
