﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace UtilisateursMicroService.Models.EntityFramework
{
    [Table("t_a_ami_ami")]
    public class Ami
    {
        [Key]
        [Column("utl_id")]
        public int UtilisateurId { get; set; }

        [Key]
        [Column("ami_id")]
        public int AmiId { get; set; }

        [ForeignKey("UtilisateurId")]
        [InverseProperty("Amis")]
        public virtual Utilisateur? UtilisateurAmi { get; set; } = null!;

        [ForeignKey("AmiId")]
        [InverseProperty("EstAmiDe")]
        public virtual Utilisateur? SonAmi { get; set; } = null!;
    }
}
