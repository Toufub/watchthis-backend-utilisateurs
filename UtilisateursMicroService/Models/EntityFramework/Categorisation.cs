﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace UtilisateursMicroService.Models.EntityFramework
{
    [Table("t_j_categorisation_ctg")]

    public class Categorisation
    {

        [Key]
        [Column("flm_id")]
        [StringLength(15)]
        public string FilmId { get; set; }

        [Key]
        [Column("utl_id")]
        [JsonIgnore]
        public int UtilisateurId { get; set; }

        [ForeignKey("UtilisateurId")]
        [InverseProperty("CategorisationsUtilisateur")]
        [JsonIgnore]
        public virtual Utilisateur? UtilisateurCategorisant { get; set; }

        [Required]
        [Column("ctg_categorie")]
        public Categorie Categorie { get; set; }

    }

    public enum Categorie
    {
        Voir,
        DejaVu,
        Non
    }
}
