﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace UtilisateursMicroService.Models.EntityFramework;

public partial class UtilisateursContext : DbContext

{

    public static readonly ILoggerFactory MyLoggerFactory = LoggerFactory.Create(builder => builder.AddConsole());

    public UtilisateursContext()
    {
    }

    public UtilisateursContext(DbContextOptions<UtilisateursContext> options)
        : base(options)
    {
    }
    public virtual DbSet<Utilisateur> Utilisateurs { get; set; } = null!;
    public virtual DbSet<Ami> Amis { get; set; } = null!;
    public virtual DbSet<Categorisation> Categorisations { get; set; } = null!;

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        if (!optionsBuilder.IsConfigured)
        {
            optionsBuilder.UseLoggerFactory(MyLoggerFactory).EnableSensitiveDataLogging();
        }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.HasDefaultSchema("public");

        modelBuilder.Entity<Categorisation>(entity =>
        {
            entity.HasKey(e => new { e.FilmId, e.UtilisateurId })
                .HasName("pk_categorisation");

            entity.HasOne(d => d.UtilisateurCategorisant)
                .WithMany(p => p.CategorisationsUtilisateur)
                .HasForeignKey(d => d.UtilisateurId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_ctg_utl");
            
            entity.Property(e => e.Categorie)
                .HasConversion(
                    v => v.ToString(),
                    v => (Categorie)Enum.Parse(typeof(Categorie), v)
                );
        });


        modelBuilder.Entity<Utilisateur>(entity =>
        {
            entity.HasMany(u => u.CategorisationsUtilisateur)
                .WithOne(c => c.UtilisateurCategorisant)
                .OnDelete(DeleteBehavior.Cascade);
            entity.Property(u => u.DateCreation).HasColumnType("Date");
            entity.Property(u => u.DateCreation).HasDefaultValueSql("now()");
        });

        modelBuilder.Entity<Ami>(entity =>
        {
            entity.HasKey(e => new { e.UtilisateurId, e.AmiId })
                .HasName("pk_ami");

            entity.HasOne(a => a.UtilisateurAmi)
                .WithMany(u => u.Amis)
                .HasForeignKey(a => a.UtilisateurId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_ami_utl");

            entity.HasOne(a => a.SonAmi)
                .WithMany(u => u.EstAmiDe)
                .HasForeignKey(a => a.AmiId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("fk_utl_ami");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
