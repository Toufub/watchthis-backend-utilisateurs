﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Intrinsics.X86;

namespace UtilisateursMicroService.Models.EntityFramework
{
    [Table("t_e_utilisateur_utl")]
    [Index(nameof(Mail), IsUnique = true)]
    public partial class Utilisateur
    {
        public Utilisateur()
        {
            CategorisationsUtilisateur = new HashSet<Categorisation>();
            Amis = new HashSet<Ami>();
            EstAmiDe = new HashSet<Ami>();
            NotificationActive = false;
        }

        [Key]
        [Column("utl_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UtilisateurId { get; set; }

        [Column("utl_betaseriesid")]
        [StringLength(25)]
        public string? BetaSeriesId { get; set; }

        [Required]
        [Column("utl_nom")]
        [StringLength(50)]
        public String Nom { get; set; }

        [Required]
        [Column("utl_prenom")]
        [StringLength(50)]
        public String Prenom { get; set; }

        [Required]
        [Column("utl_mail")]
        [EmailAddress]
        [StringLength(100, MinimumLength = 6, ErrorMessage = "La longueur d’un email doit être comprise entre 6 et 100 caractères.")]
        public String Mail { get; set; }

        [Column("utl_pwd")]
        [StringLength(64)]
        //[JsonIgnore]
        public string? Pwd { get; set; }

        [Column("utl_datecreation")]
        public DateTime DateCreation { get; set; }

        [Column("utl_imageprofil")]
        [StringLength(255)]
        public string? ImageProfil { get; set; }

        [Column("utl_notificationactive")]
        public Boolean NotificationActive { get; set; }

        [InverseProperty("UtilisateurAmi")]
        [JsonIgnore]
        public virtual ICollection<Ami> Amis { get; set; }

        [InverseProperty("SonAmi")]
        [JsonIgnore]
        public virtual ICollection<Ami> EstAmiDe { get; set; }

        [InverseProperty("UtilisateurCategorisant")]
        public virtual ICollection<Categorisation> CategorisationsUtilisateur { get; set; }
    }
}
