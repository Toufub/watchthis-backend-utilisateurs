﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UtilisateursMicroService.Models.DataManager;
using UtilisateursMicroService.Models.EntityFramework;
using UtilisateursMicroService.Models.Repository;
using UtilisateursMicroService.Services;

namespace UtilisateursMicroService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class AmisController : ControllerBase
    {
        private readonly IAmiRepository<Ami> dataRepository;
        private readonly IUtilisateurRepository<Utilisateur> utilisateurRepository;
        private readonly IConfiguration _config;

        public AmisController(IAmiRepository<Ami> dataRepository, IUtilisateurRepository<Utilisateur> utilisateurRepository,
            IConfiguration config)
        {
            this.dataRepository = dataRepository;
            this._config = config;
            this.utilisateurRepository = utilisateurRepository;
        }

        // GET: api/Amis
        [HttpGet("[action]")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<Ami>>> GetAmis()
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            return await dataRepository.GetAllAmis(tokenId);
        }

        // GET: api/Amis
        [HttpGet("[action]")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<Ami>>> GetAmiDe()
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            return await dataRepository.GetAllAmisDe(tokenId);
        }

        // GET: api/Amis
        [HttpGet("[action]/{amiId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Ami>> GetAmi(int amiId)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            var ami = await dataRepository.GetAmi(tokenId, amiId);

            if (ami == null)
            {
                return NotFound();
            }

            return ami;
        }

        // POST: api/Amis
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Ami>> PostAmi(Ami ami)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            var utilisateurAmi = await utilisateurRepository.GetById(ami.AmiId);
            if (utilisateurAmi.Value == null)
            {
                return NotFound("Ami non trouvé");
            }
            else
            {
                if (tokenId == ami.AmiId)
                {
                    return BadRequest("Ami de vous même ?");
                }
                else
                {
                    var amiExistant = await dataRepository.GetAmi(tokenId, ami.AmiId);
                    if (amiExistant.Value == null)
                    {
                        ami.UtilisateurId = tokenId;
                        dataRepository.Add(ami);
                    }
                    else
                    {
                        return BadRequest("Déjà amis");
                    }
                }
            } 
            return CreatedAtAction("GetAmi", new {amiId = ami.AmiId}, ami);
        }

        // DELETE: api/Amis/5
        [HttpDelete("{amiId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteAmi(int amiId)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            var ami = await dataRepository.GetAmi(tokenId, amiId);
            if (ami == null) {
                return NotFound();
            } else {
                dataRepository.Delete(ami.Value);
                return NoContent();
            }
        }
    }
}
