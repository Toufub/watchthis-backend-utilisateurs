﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using FilmsMicroService.Models.Dto;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NuGet.Protocol;
using UtilisateursMicroService.Models;
using UtilisateursMicroService.Models.DataManager;
using UtilisateursMicroService.Models.EntityFramework;
using UtilisateursMicroService.Models.Repository;
using UtilisateursMicroService.Services;

namespace UtilisateursMicroService.Controllers
{
    [Route("")]
    [ApiController]
    public class UtilisateursController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IUtilisateurRepository<Utilisateur> dataRepository;

        public UtilisateursController(IUtilisateurRepository<Utilisateur> dataRepository, IConfiguration config)
        {
            this.dataRepository = dataRepository;
            _config = config;

        }

        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<Utilisateur>>> GetUtilisateurs()
        {
            return await dataRepository.GetAll();
        }

        [Route("[action]/{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Utilisateur>> ById(int id)
        {
            var utilisateur = await dataRepository.GetById(id);
            if (utilisateur.Value == null)
            {
                return NotFound("ID utilisateur inconnu");
            }
            return utilisateur;
        }

        // GET: api/Utilisateurs/test@test.com
        [Route("[action]/{mail}")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Utilisateur>> ByMail(string mail)
        {
            var utilisateur = await dataRepository.GetByString(mail);
            if (utilisateur.Value == null)
            {
                return NotFound("Mail utilisateur inconnu");
            }
            return utilisateur;
        }

        [Route("[action]/{search}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<Utilisateur>>> Search(string search)
        {
            return await dataRepository.Search(search);
        }
        // PUT: api/Utilisateurs/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Utilisateur>> PutUtilisateur(int id, Utilisateur utilisateur)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            var userToUpdate = await dataRepository.GetById(id);
            if (id != utilisateur.UtilisateurId)
            {
                return BadRequest("ID ne correspond pas à cet utilisateur");
            } else if(id != tokenId)
            {
                return Unauthorized();
            } else if (userToUpdate == null)
            {
                return NotFound();
            }

            await dataRepository.Update(userToUpdate.Value, utilisateur);

            return await dataRepository.GetById(id);
        }

        [HttpPut("[action]/{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<Utilisateur>> Password(int id, ChangePassword changePassword)
        {
            var entity = (await dataRepository.GetById(id)).Value;
            var entityToUpdate = await dataRepository.GetById(id);

            if (entityToUpdate == null)
            {
                return NotFound();
            }

            if(Encoding.ASCII.GetString(PasswordManager.HashPassword(changePassword.OldPwd)) == entity.Pwd)
            {
                entity.Pwd = Encoding.ASCII.GetString(PasswordManager.HashPassword(changePassword.NewPwd));
                await dataRepository.UpdatePwd(entityToUpdate.Value, entity);
            }

            return await dataRepository.GetById(id);
        }

        // GET: api/Films/5
        [HttpPost("[action]")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<Utilisateur>>> ByIds(ListUtilisateurId list)
        {
            List<Utilisateur> utilisateurs = new List<Utilisateur>();
            foreach (var id in list.ids)
            {
                var utilisateur = await dataRepository.GetById(id);
                var utilisateurToAdd = utilisateur.Value;
                if (utilisateurToAdd != null)
                {
                    utilisateurs.Add(utilisateurToAdd);
                }
            }
            return utilisateurs;
        }
        // POST: api/Utilisateurs
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost("[action]")]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Utilisateur>> VerifyPassword([FromBody] LoginForm loginForm)
        {
            var utilisateur = await dataRepository.GetByString(loginForm.Mail);
            if (utilisateur.Value == null)
            {
                return NotFound();
            }
            else
            {
                var utilisateurConnect = PasswordManager.VerifyPassword(loginForm.Pwd, utilisateur.Value);
                if (utilisateurConnect == null)
                {
                    return Unauthorized();
                }
                else
                {
                    return utilisateurConnect;
                }
            }
        }

        // POST: api/Utilisateurs
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Utilisateur>> PostUtilisateur(Utilisateur utilisateur)
        {
            var utilisateur_exist = await dataRepository.GetByString(utilisateur.Mail);
            if (utilisateur_exist.Value == null) {
                var login = new LoginForm(utilisateur.Mail, utilisateur.Pwd);
                dataRepository.Add(utilisateur).Wait();
                HttpContent content = Connect.Post("auth/", JsonSerializer.Serialize(login));
                if(content == null)
                {
                    return Unauthorized();
                } else
                {
                    return Ok(await content.ReadAsStringAsync());
                }
            } else {
                return BadRequest("Adresse Mail déjà utilisée");
            }
        }

        // DELETE: api/Utilisateurs/5
        [HttpDelete("{id}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeleteUtilisateur(int id)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);

            if (tokenId == id)
            {
                var utilisateur = await dataRepository.GetById(id);
                var real_utilisateur = utilisateur.Value;
                if (real_utilisateur == null)
                {
                    return NotFound();
                }
                await dataRepository.Delete(real_utilisateur);
                return NoContent();
            } else
            {
                return Unauthorized();
            }
        }
    }
}
