﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Hosting;
using UtilisateursMicroService.Models.EntityFramework;
using UtilisateursMicroService.Models;
using UtilisateursMicroService.Services;
using System.Text.Json;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using UtilisateursMicroService.Models.Repository;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;
using System.Text.Json.Nodes;
using Microsoft.CodeAnalysis.VisualBasic.Syntax;

namespace UtilisateursMicroService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BetaSeriesController : ControllerBase
    {
        private readonly IConfiguration _config;
        private readonly IUtilisateurRepository<Utilisateur> dataRepository;
        private readonly UtilisateursContext? utilisateursContext;
        private readonly ICategorisationRepository<Categorisation> categorisationRepository;


        public BetaSeriesController(IUtilisateurRepository<Utilisateur> dataRepository,
            ICategorisationRepository<Categorisation> categorisationRepository,
            UtilisateursContext context, IConfiguration config)
        {
            this.dataRepository = dataRepository;
            this.utilisateursContext = context;
            _config = config;
            this.categorisationRepository = categorisationRepository;
        }

        // POST: api/Utilisateurs/betaseries
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Synchronise([FromBody] BetaSeriesAuth auth)
        {
            Authenticator.init(_config);
            BetaSeriesAPI.init(_config, utilisateursContext, categorisationRepository);

            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            var utilisateur = (await dataRepository.GetById(tokenId)).Value;

            int codErr = await BetaSeriesAPI.sync(utilisateur, auth);

            if(codErr == -1)
            {
                return NotFound("Utilisateur non trouvé sur BetaSeries");
            }
            return NoContent();
        }

        [HttpGet]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> Refresh()
        {
            Authenticator.init(_config);
            BetaSeriesAPI.init(_config, utilisateursContext, categorisationRepository);

            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            var utilisateur = (await dataRepository.GetById(tokenId)).Value;

            int codErr = await BetaSeriesAPI.sync(utilisateur);

            if(codErr == -2)
            {
                return BadRequest("Betaseries n'est pas synchronisé");
            }

            return NoContent();
        }

    }
}
