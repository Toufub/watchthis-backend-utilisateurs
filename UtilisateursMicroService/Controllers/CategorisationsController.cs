﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using UtilisateursMicroService.Models.DataManager;
using UtilisateursMicroService.Models.EntityFramework;
using UtilisateursMicroService.Models.Repository;
using UtilisateursMicroService.Services;

namespace UtilisateursMicroService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategorisationsController : ControllerBase
    {
        private readonly ICategorisationRepository<Categorisation> dataRepository;
        private IConfiguration _config;

        public CategorisationsController(ICategorisationRepository<Categorisation> dataRepository, IConfiguration config)
        {
            this.dataRepository = dataRepository;
            this._config = config;
        }

        // GET: api/Categorisations
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Categorisation>>> GetCategorisations()
        {
            return await dataRepository.GetAll();
        }

        // GET: api/Categorisations/5
        [Route("{utilisateurId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<Categorisation>>> GetCategorisationsByUtilisateur(int utilisateurId)
        {
            return await dataRepository.GetByUtilisateurId(utilisateurId);
        }

        [Route("[action]/{categorie}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<IEnumerable<Categorisation>>> ByCategorieUtilisateur(String categorie)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            return await dataRepository.GetByCategorieAndUtilisateurId(categorie, tokenId);
        }

        [Route("[action]/{filmId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Categorisation>> ByFilmUtilisateur(string filmId)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            var categorisation = await dataRepository.GetOneByFilmIdAndUtilisateurId(filmId, tokenId);

            if (categorisation == null)
            {
                return NotFound();
            }

            return categorisation;
        }


        // PUT: api/Categorisations/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{utilisateurId}/{filmId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> PutCategorisation(int utilisateurId, string filmId, [FromBody] Categorisation categorisation)
        {
            var categorisationToUpdate = (await dataRepository.GetOneByFilmIdAndUtilisateurId(filmId, utilisateurId)).Value;
            if (categorisationToUpdate == null)
            {
                return NotFound();
            } else
            {
                Authenticator.init(_config);
                JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
                var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
                if (filmId != categorisation.FilmId)
                {
                    return BadRequest("Ids ne correspondent pas.");
                }
                else if (tokenId != utilisateurId)
                {
                    return Unauthorized();
                }
                categorisation.UtilisateurId = utilisateurId;
                await dataRepository.Update(categorisationToUpdate, categorisation);
            }

            return NoContent();
        }

        // POST: api/Categorisations
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<ActionResult<Categorisation>> PostCategorisation(Categorisation categorisation)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            categorisation.UtilisateurId = tokenId;
            try
            {
                await dataRepository.Add(categorisation);
                return CreatedAtAction("ByFilmUtilisateur",
                   new { filmId = categorisation.FilmId },
                   categorisation);
            }
            catch (Exception e)
            {
                return BadRequest("Categorisation déjà existante");
            }
        }

        // DELETE: api/Categorisations/5
        [HttpDelete("{utilisateurId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeleteCategorisations(int utilisateurId)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            var categorisations = await dataRepository.GetByUtilisateurId(utilisateurId);
            if (tokenId != utilisateurId)
            {
                return Unauthorized();
            }
            foreach (Categorisation categorisation in categorisations.Value)
            {
                await dataRepository.Delete(categorisation);
            }
            return NoContent();
        }

        // DELETE: api/Categorisations/5
        [HttpDelete("{utilisateurId}/{filmId}")]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> DeleteCategorisation(int utilisateurId, string filmId)
        {
            Authenticator.init(_config);
            JwtSecurityToken token = Authenticator.GetToken(HttpContext.Request.Headers["Authorization"]);
            var tokenId = int.Parse(token.Claims.First(x => x.Type == ClaimTypes.NameIdentifier).Value);
            var categorisation = await dataRepository.GetOneByFilmIdAndUtilisateurId(filmId, utilisateurId);
            if (categorisation.Value == null)
            {
                return NotFound();
            } else if(tokenId != utilisateurId)
            {
                return Unauthorized();
            } else
            {
                await dataRepository.Delete(categorisation.Value);
            }
            return NoContent();
        }
    }
}
