﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Reflection.Metadata;
using System.Text.Json;
using System.Text.Json.Nodes;
using UtilisateursMicroService.Models;
using UtilisateursMicroService.Models.EntityFramework;
using UtilisateursMicroService.Models.Repository;

namespace UtilisateursMicroService.Services
{
    public class BetaSeriesAPI
    {

        private static IConfiguration _config;
        private static UtilisateursContext utilisateursContext;
        private static ICategorisationRepository<Categorisation> categorisationRepository;

        public static void init(IConfiguration config, UtilisateursContext _utilisateursContext,
             ICategorisationRepository<Categorisation> _categorisationRepository)
        {
            _config = config;
            utilisateursContext = _utilisateursContext;
            categorisationRepository = _categorisationRepository;
        }

        public static async Task<int> sync(Utilisateur utilisateur, BetaSeriesAuth auth = null)  
        {
            string idBetaSeries = utilisateur.BetaSeriesId;
            if (auth != null && idBetaSeries == null)
            {
                idBetaSeries = await AuthBetaSeries(utilisateur, auth);
                if (idBetaSeries == null)
                {
                    return -1; //identifiant incorrect (betaseries)
                }

                utilisateursContext.Entry(utilisateur).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                utilisateur.BetaSeriesId = idBetaSeries;
            } else if(idBetaSeries == null)
            {
                return -2; //besoin de se connecter
            }
            
            utilisateur.ImageProfil = await GetBetaSeriesImage(utilisateur);
            await utilisateursContext.SaveChangesAsync();

            List<string> filmsIds_avoir = (await GetBetaSeriesFilms(idBetaSeries, 0)).ToList() ;
            List<string> filmsIds_vu = (await GetBetaSeriesFilms(idBetaSeries, 1)).ToList();
            List<string> filmsIds_nepasvoir = (await GetBetaSeriesFilms(idBetaSeries, 2)).ToList();

            await UpdateCategorisation(filmsIds_vu, utilisateur.UtilisateurId, Categorie.DejaVu);
            await UpdateCategorisation(filmsIds_avoir, utilisateur.UtilisateurId, Categorie.Voir);
            await UpdateCategorisation(filmsIds_nepasvoir, utilisateur.UtilisateurId, Categorie.Non);

            return 0;
        }

        public static async Task<string> AuthBetaSeries(Utilisateur utilisateur, BetaSeriesAuth auth)
        {
            auth.password = MD5Hash.Hash.Content(auth.password);
            auth.client_id = _config["BetaSeries:API_Key"];
            HttpContent content = Connect.Post(_config["BetaSeries:API_Url"] + "members/auth", JsonSerializer.Serialize(auth));
            if (content == null)
            {
                return null;
            }
            JsonNode data = await JsonSerializer.DeserializeAsync<JsonNode>(await content.ReadAsStreamAsync());
            return ((int)data["user"]["id"]).ToString();
        }

        public static async Task<string> GetBetaSeriesImage(Utilisateur utilisateur)
        {
            string parametres = "client_id=" + _config["BetaSeries:API_Key"] + "&id=" + utilisateur.BetaSeriesId;
            HttpContent content = Connect.Get(_config["BetaSeries:API_Url"] + "members/infos?" + parametres);
            if (content == null)
            {
                return null;
            }
            JsonNode data = await JsonSerializer.DeserializeAsync<JsonNode>(await content.ReadAsStreamAsync());
            if(data["member"]["avatar"] != null)
            {
                return (string)data["member"]["avatar"].ToString();
            } else
            {
                return null;
            }
        }

        public static async Task<IEnumerable<string>> GetBetaSeriesFilms(string idBetaSeries, int state)
        {
            List<string> filmsIds = new List<string>();
            string parametres = "client_id=" + _config["BetaSeries:API_Key"] + "&id=" + idBetaSeries.ToString() 
                + "&state=" + state.ToString();

            HttpContent content = Connect.Get(_config["BetaSeries:API_Url"] + "movies/member?" + parametres);
            if (content == null)
            {
                return filmsIds;
            }
            JsonNode data = await JsonSerializer.DeserializeAsync<JsonNode>(await content.ReadAsStreamAsync());
            JsonNode movies = data["movies"];

            foreach (JsonNode movie in movies.AsArray())
            {
                filmsIds.Add(movie["imdb_id"].ToString());
            }
            return filmsIds;
        }

        public static async Task UpdateCategorisation(List<string> filmsIds, int utilisateurId, Categorie categorie)
        {
            foreach (string filmId in filmsIds)
            {
                var exist = await utilisateursContext.Categorisations
                            .Where(c => c.UtilisateurId == utilisateurId)
                            .Where(c => c.FilmId == filmId)
                            .FirstOrDefaultAsync();
                if (exist == null) {
                    Categorisation categorisation = new Categorisation();
                    categorisation.UtilisateurId = utilisateurId;
                    categorisation.FilmId = filmId;
                    categorisation.Categorie = categorie;
                    await utilisateursContext.Categorisations.AddAsync(categorisation);
                    await utilisateursContext.SaveChangesAsync();
                }
            }
        }
    }
}
