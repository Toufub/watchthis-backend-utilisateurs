﻿using System.Security.Cryptography;
using System.Text;
using UtilisateursMicroService.Models.EntityFramework;

namespace UtilisateursMicroService.Services
{
    public class PasswordManager
    {
        public static byte[] HashPassword(string input)
        {
            return SHA256.HashData(Encoding.ASCII.GetBytes(input));
        }
        public static Utilisateur VerifyPassword(string pwd, Utilisateur utilisateur)
        {
            if (pwd == utilisateur.Pwd)
            {
                return utilisateur;
            }
            else
            {
                return null;
            }
        }
    }
}
