﻿using Microsoft.AspNetCore.Mvc;
using System.Net;
using System.Text.Json.Nodes;
using System.Text;
using System.Threading.Tasks;

namespace UtilisateursMicroService.Services
{
    class Connect
    {
        private static readonly string ApiURL = "http://reverseproxy:8080/api/";

        public Connect()
        {

        }

        public static HttpContent Get(string route)
        {
            var client = new HttpClient();
            if (route.Contains("https://") == false)
            {
                route = ApiURL + route;
            }
            Task<HttpResponseMessage> task = client.GetAsync(route);
            task.Wait();
            if (task.Result.IsSuccessStatusCode)
            {
                return task.Result.Content;
            }
            else
            {
                return null;
            }
        }

        public static HttpContent Post(string route, string jsonObject)
        {
            var client = new HttpClient();
            var content = new StringContent(jsonObject, Encoding.UTF8, "application/json");
            if(route.Contains("https://") == false)
            {
                route = ApiURL + route;
            }
            Task<HttpResponseMessage> task = client.PostAsync(route, content);
            task.Wait();
            if (task.Result.IsSuccessStatusCode)
            {
                return task.Result.Content;
            }
            else
            {
                return null;
            }
        }

    }
}
