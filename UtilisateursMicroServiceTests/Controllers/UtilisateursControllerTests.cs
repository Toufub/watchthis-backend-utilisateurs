﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UtilisateursMicroService.Controllers;
using UtilisateursMicroService.Models.DataManager;
using UtilisateursMicroService.Models.EntityFramework;
using UtilisateursMicroService.Models.Repository;

namespace UtilisateursMicroServiceTests.Controllers
{
    [TestClass()]
    public class UtilisateursControllerTests
    {
        private readonly UtilisateursController _controller;
        private readonly UtilisateursContext _context;
        private IUtilisateurRepository<Utilisateur> _dataRepository;
        private IConfiguration _config;
        public UtilisateursControllerTests(IConfiguration config)
        {
            var npgsql = "Server=postgres-utilisateurs;port=5432;Database=utilisateurs; uid=utilisateurs-user; password=utilisateurs-user;";
            var builder = new DbContextOptionsBuilder<UtilisateursContext>().UseNpgsql(npgsql);
            this._config = config;
            this._context = new UtilisateursContext(builder.Options);
            this._dataRepository = new UtilisateurManager(_context);
            this._controller = new UtilisateursController(_dataRepository, _config);
        }
        /*
        [TestMethod()]
        public void GetUtilisateursTest()
        {
            Task<ActionResult<IEnumerable<Utilisateur>>> task = this._controller.GetUtilisateurs();
            task.Wait();
            var users_from_get = task.Result.Value.ToArray();
            var users_from_db = this._context.Utilisateurs.ToArray();
            Assert.IsTrue(users_from_get.SequenceEqual(users_from_db));
        }

        [TestMethod()]
        public void GetUtilisateurByIdTest()
        {
            Task<ActionResult<Utilisateur>> task = this._controller.ById(7);
            task.Wait();
            var user_from_get = task.Result.Value;
            var user_from_db = this._context.Utilisateurs
                        .Where(u => u.UtilisateurId == 7)
                       .FirstOrDefault();
            var different_user_from_db = this._context.Utilisateurs
                        .Where(u => u.UtilisateurId == 9)
                       .FirstOrDefault();
            Assert.IsTrue(user_from_db == user_from_get);
            Assert.IsFalse(different_user_from_db == user_from_get);
        }

        [TestMethod()]
        public void GetUtilisateurByEmailTest()
        {
            Task<ActionResult<Utilisateur>> task = this._controller.ByMail("test@test.com");
            task.Wait();
            var user_from_get = task.Result.Value;
            var user_from_db = this._context.Utilisateurs
                .Where(u => u.Mail.ToUpper().Equals(("test@test.com").ToUpper()))
                .FirstOrDefault();
            var different_user_from_db = this._context.Utilisateurs
                .Where(u => u.Mail.ToUpper().Equals(("eloy62610@gmail.com").ToUpper()))
                .FirstOrDefault();
            Assert.IsTrue(user_from_db == user_from_get);
            Assert.IsFalse(different_user_from_db == user_from_get);
        }

        [TestMethod]
        public void Postutilisateur_ModelValidated_CreationOK()
        {
            Random rnd = new Random();
            int chiffre = rnd.Next(1, 1000000000);
            Utilisateur userAtester = new Utilisateur()
            {
                Nom = "MACHIN",
                Prenom = "Luc",
                Mail = "machin" + chiffre + "@gmail.com",
                Pwd = "Toto1234!"
            };
            // Act
            var result = _controller.PostUtilisateur(userAtester).Result;
            // .Result pour appeler la méthode async de manière synchrone, afin d'obtenir le résultat
            var result2 = _controller.ByMail(userAtester.Mail);
            var actionResult = result2.Result as ActionResult<Utilisateur>;
            // Assert
            Assert.IsInstanceOfType(actionResult.Value, typeof(Utilisateur), "Pas un utilisateur");
            Utilisateur? userRecupere = _context.Utilisateurs.Where(u => u.Mail.ToUpper() == userAtester.Mail.ToUpper()).FirstOrDefault();
            // On ne connait pas l'ID de l’utilisateur envoyé car numéro automatique.
            // Du coup, on récupère l'ID de celui récupéré et on compare ensuite les 2 users
            userAtester.UtilisateurId = userRecupere.UtilisateurId;
            Assert.AreEqual(userRecupere, userAtester, "Utilisateurs pas identiques");
        }*/
    }
}
