# WatchThis - Backend Utilisateurs

## Fonctionnement API Utilisateurs
### Find All - GET

Necessite un JWT token
Code de retour :
* 200 : OK
* 401 : Unauthorized

`/api/utilisateurs/`

Structure d'un utilisateur : 
````
{
	"utilisateurId":  7,
	"betaSeriesId":  null,
	"nom":  "test",
	"prenom":  "test",
	"mail":  "test@test.com",
	"pwd":")??#???x??=??]E%{? ?>,??{??d\\??QG??mQ??}??|?||n.?)?l??T?i?",
	"dateCreation":  "2023-01-10T00:00:00",
	"imageProfil":  null,
	"notificationActive":  false,
	"amis":  [],
	"estAmiDe":  [],
	"categorisationsUtilisateur":  []
}
````
### Find One By Id - GET

Necessite un JWT token
Code de retour :
* 200 : OK
* 404 : Not found
* 401 : Unauthorized

`/api/utilisateurs/search/{search}`

### Find One By Mail - GET
Code de retour :
* 200 : OK
* 401 : Unauthorized


`/api/utilisateurs/byid/{id}`

### Find One By Mail - GET
Code de retour :
* 200 : OK
* 404 : Not found

`/api/utilisateurs/bymail/{mail}`

### New - POST
Code de retour :
* 201 : Created
* 400 : Bad request (email déjà utilisé)

`api/utilisateurs/`

Structure de données : 
````
{
	"Nom":  "Cambray",
	"Prenom":  "eloi",
	"Mail":  "eloy62610@gmail.com",
	"Pwd":  "t3St!@"
}
````

Renvoie un JWT token (auth)

### Edit - PUT
Necessite un JWT token
Code de retour :
* 200 : OK
* 404 : Not found
* 401 : Unauthorized (token != id)

`api/utilisateurs/{id}`

Renvoie l'utilisateur à jour

Structure de données
````
{
	"UtilisateurId":  9,
	"Nom":  "Cambray",
	"Prenom":  "Eloi",
	"Mail":  "eloy62610@gmail.com"
}
````

### Password - PUT
Necessite un JWT token
Code de retour :
* 200 : OK
* 404 : Not found
* 401 : Unauthorized (token != id)

`api/utilisateurs/password/{id}`

Renvoie l'utilisateur à jour

Structure de données
````
{
	"OldPwd":  old_pwd,
	"NewPwd":  new_pwd
}
````

### Delete - DELETE
Necessite un JWT token
Code de retour :
* 404 : not found
* 204 : No content
* 401 : Unauthorized (token != id)

`api/utilisateurs/{id}`

## Fonctionnement API Categorisation
### Find All - GET

`/api/utilisateurs/categorisations`

Structure d'une categorisation: 
````
{
	"filmId":  1,
	"utilisateurId":  7,
	"categorie":  0
	//2 = pas interesse, 0 = à voir, 1 = Déjà vu 
}
````

### Find All By Id utilisateur - GET

`/api/utilisateurs/categorisations/{id}`

### Find All By Id utilisateur & categorie - GET

Necessite un JWT token
Code de retour :
* 200 : OK
* 401 : Unauthorized

`/api/utilisateurs/categorisations/ByCategorieUtilisateur/{categorie : Non/DejaVu/Voir}`

### Find One By Id Utilisateur & Id Film - GET
Necessite un JWT token
Code de retour :
* 200 : OK
* 404 : Not found
* 401 : Unauthorized

`/api/utilisateurs/categorisations/ByFilmUtilisateur/{filmId}`

### New - POST
Necessite un JWT Token
Code de retour :
* 201 : Created
* 400 : Bad request (existe déjà pour ce couple d'id)
* 401 : Unauthorized (utilisateur ID != tokenId)
`api/utilisateurs/categorisations`
Structure de données : 
````
{
	"filmId":  1,
	"utilisateurId":  7,
	"categorie":  0
}
````
### Edit - PUT
Necessite un JWT Token
Code de retour :
* 204 : No content
* 401 : Unauthorized (utilisateurId != tokenId)
* 400 Bad request (id incorrect)

`api/utilisateurs/categorisations/{utilisateurId}/{filmId}`

### Remove - DELETE
Necessite un JWT Token
Code de retour :
* 204 : no content
* 404 : Not found
* 401 : Unauthorized (utilisateurId != tokenId)
`/api/utilisateurs/categorisations/{utilisateurId}/{filmId}`

## Fonctionnement API Amis
### Find Amis - GET

`/api/utilisateurs/amis/getamis`

Necessite un JWT token
Code de retour :
* 200 : OK
* 401 : Unauthorized

Structure d'un ami: 
````
    {
        "utilisateurId": 11,
        "amiId": 7,
        "utilisateurAmi": null,
        "sonAmi": {
            "utilisateurId": 7,
            "betaSeriesId": null,
            "nom": "test",
            "prenom": "test",
            "mail": "test@test.com",
            "pwd":")??#???x??=??]E%{? ?>,??{??d\\??QG??mQ??}??|?||n.?)?l??T?i?",
            "dateCreation": "2023-01-10T00:00:00",
            "imageProfil": null,
            "notificationActive": false,
            "categorisationsUtilisateur": []
        }
    }
````

### Find Amis - GET

`/api/utilisateurs/amis/getamide`

Necessite un JWT token
Code de retour :
* 200 : OK
* 401 : Unauthorized

Structure d'un amiDe: 
````
    {
        "utilisateurId": 11,
        "amiId": 9,
        "utilisateurAmi": {
            "utilisateurId": 11,
            "betaSeriesId": null,
            "nom": "Cambray",
            "prenom": "Eloi",
            "mail": "eloitest@gmail.com",
            "pwd":"?U??????[6????%??w??_??????V??",
            "dateCreation": "2023-01-12T00:00:00",
            "imageProfil": null,
            "notificationActive": false,
            "categorisationsUtilisateur": []
        },
        "sonAmi": null
    }
````

### New ami - POST

Code de retour :
* 200 : OK
* 401 : Unauthorized
* 404 : Not Found
* 400 : Bad Request

`/api/utilisateurs/amis`

Structure à envoyer :
````
{
    "amiId": "9"
}
````

### Supprimer Ami - DELETE

Necessite un JWT token
Code de retour :
* 204 : NoContent
* 401 : Unauthorized
* 404 : NotFound

`/api/utilisateurs/amis/{idUtilisateur}`

## Fonctionnement API BetaSeries

### Auth - POST

Necessite un JWT token
Code de retour :
* 204 : NoContent
* 401 : Unauthorized
* 404 : NotFound

`/api/utilisateurs/betaseries/`

Structure à envoyer :
````
{
    "login": user_beta_serie, 
    "password": password_beta_serie
}
````